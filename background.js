//Script to inject customized gremlins.js script into the website

chrome.runtime.onConnect.addListener(function(port) {
  console.assert(port.name == "port1");
  port.onMessage.addListener(function(msg) {
  	console.log('Inside background.js');
    if (msg.joke == "Knock knock"){
    	var codeStr = "gremlins.createHorde().gremlin(gremlins.species.formFiller()).gremlin(gremlins.species.clicker().clickTypes(['click'])).gremlin(gremlins.species.toucher()).gremlin(gremlins.species.scroller()).gremlin(function() {window.$ = function() {};}).unleash({nb: 10000});"
	chrome.tabs.executeScript({
		file: 'gremlins.min.js'
	});
	chrome.tabs.executeScript({
		code: codeStr
	});
    }
  });
});
